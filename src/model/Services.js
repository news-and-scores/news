import * as Errors from "../Errors"
import * as Events from "./Events"
import * as EventStore from "../port/EventStore"


let registerFeedProjectionState =
    undefined;

const refresh = () => {
    registerFeedProjectionState = registerFeedProjectionState
        .then(state =>
            EventStore.events({}, state.from)
                .then(events => {
                    events.forEach(event => {
                        state.from = event.id;

                        switch (event.name) {
                            case 'FeedRegistered':
                                state.feedIds.add(event.data.feedID);
                                break;

                            case 'ArticlePublished':
                                state.articleIds.add(event.data.articleID);
                                break;
                        }
                    });

                    return (events.length === 1000)
                        ? refresh()
                        : state;
                }));

    return registerFeedProjectionState;
};


const registerFeedImpl = content =>
    refresh().then(state =>
        state.feedIds.has(content.feedID)
            ? Promise.reject(Errors.duplicateFeedID(content.feedID, content))
            : EventStore.publish([
                Events.feedRegistered(content.feedID, content.source, content.title, content.description, content.link, content.copyright, content.image)
            ])
    );


const recordArticleImpl = content =>
    refresh().then(state =>
        !state.feedIds.has(content.feedID) ? Promise.reject(Errors.unknownFeedID(content.feedID, content))
            : state.articleIds.has(content.articleID) ? Promise.reject(Errors.duplicateArticleID(content.articleID, content))
            : EventStore.publish([Events.articlePublished(content.articleID, content.title, content.description, content.url, content.publishedDate, content.feedID)])
    );


export let registerFeed =
    undefined;


export let recordArticle =
    undefined;


export const __reset_all__ = () => {
    registerFeedProjectionState =
        Promise.resolve({
            from: undefined,
            feedIds: new Set(),
            articleIds: new Set()
        });

    registerFeed = registerFeedImpl;
    recordArticle = recordArticleImpl;
};

__reset_all__();