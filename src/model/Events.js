export const feedRegistered = (feedID, source, title, description, link, copyright, image) => {
    const event =
        {
            name: 'FeedRegistered',
            version: '1',
            data: {

            }
        };

    if (feedID !== undefined)
        event.data.feedID = feedID;

    if (source !== undefined)
        event.data.source = source;

    if (title !== undefined)
        event.data.title = title;

    if (description !== undefined)
        event.data.description = description;

    if (link !== undefined)
        event.data.link = link;

    if (copyright !== undefined)
        event.data.copyright = copyright;

    if (image !== undefined)
        event.data.image = image;

    return event;
};


export const articlePublished = (articleID, title, description, url, publishedDate, feedID) => {
    const event =
        {
            name: 'ArticlePublished',
            version: '1',
            data: {

            }
        };

    if (articleID !== undefined)
        event.data.articleID = articleID;

    if (title !== undefined)
        event.data.title = title;

    if (description !== undefined)
        event.data.description = description;

    if (url !== undefined)
        event.data.url = url;

    if (publishedDate !== undefined)
        event.data.publishedDate = publishedDate;

    if (feedID !== undefined)
        event.data.feedID = feedID;

    return event;
};