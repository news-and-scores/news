import * as API from "./port/API"


const server =
    API.start({
        host: 'localhost',
        port: 8001,
        info: true
    }).catch(console.error);


process.on('SIGINT', () => {
    console.log('Caught SIGINT: Stopping server');

    server.then(s => s.stop({timeout: 10000}).then(err => {
        console.log('Server stopped');
        process.exit(err ? 1 : 0);
    }));
});