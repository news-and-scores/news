export const noNameSuppliedInCommand = payload => ({
    type: "NoNameSuppliedInCommand",
    payload
});


export const noCommandHandlerFound = (name, payload) => ({
    type: "NoCommandHandlerFound",
    name,
    payload
});


export const handlerSchemaErrors = (payload, errors) => ({
    type: "HandlerSchemaErrors",
    payload,
    errors
});


export const duplicateFeedID = (feedID, payload) => ({
    type: "DuplicateFeedID",
    feedID,
    payload
});

export const unknownFeedID = (feedID, payload) => ({
    type: "UnknownFeedID",
    feedID,
    payload
});

export const duplicateArticleID = (articleID, payload) => ({
    type: "DuplicateArticleID",
    articleID,
    payload
});
