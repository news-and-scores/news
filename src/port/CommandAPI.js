import * as Schema from "./Schemas"
import * as Service from "../model/Services"
import * as Errors from "../Errors"


const nameCommandHandlers = ({
    RecordArticle: [Schema.validateRecordArticleCommand, () => Service.recordArticle],
    RegisterFeed: [Schema.validateRegisterFeedCommand, () => Service.registerFeed]
});


export const register = server => {
    server.route({
        method: 'POST',
        path: '/api/command',
        handler: (request, h) => {
            const payload =
                request.payload;

            const name =
                payload.name;

            if (name === undefined)
                return h.response(Errors.noNameSuppliedInCommand(payload)).code(412);

            const commandHandler =
                nameCommandHandlers[name];

            if (commandHandler === undefined)
                return h.response(Errors.noCommandHandlerFound(name, payload)).code(412);

            const errors =
                commandHandler[0](payload);

            if (errors)
                return h.response(Errors.handlerSchemaErrors(payload, errors)).code(412);

            return commandHandler[1]()(payload);
        }
    });
};
