import AJV from 'ajv/lib/ajv'
import * as FileSystem from 'fs'
import * as Path from 'path'


const ajv =
    new AJV();


const compileSchema = fileName =>
    ajv.compile(JSON.parse(FileSystem.readFileSync(Path.join(__dirname, fileName), {encoding: 'utf-8'})));


const compiledRecordArticleCommand =
    compileSchema('/schemas/RecordArticleCommand.json');

const compiledRegisterFeedSchema =
    compileSchema('/schemas/RegisterFeedCommand.json');


const validateSchema = schema => data => {
    const valid =
        schema(data);

    if (valid)
        return undefined;
    else
        return schema.errors;
};


export const validateRecordArticleCommand =
    validateSchema(compiledRecordArticleCommand);

export const validateRegisterFeedCommand =
    validateSchema(compiledRegisterFeedSchema);


