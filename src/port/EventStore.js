import fetch from 'node-fetch'


const EVENT_STORE_URI =
    process.env.EVENT_STORE_URI || "http://localhost:8000";


const eventsImpl = (query, from = undefined) => {
    let uri =
        `${EVENT_STORE_URI}/api/events?limit=1000`;

    if (query !== undefined) {
        uri = uri + `&query=${query}`;
    }
    if (from !== undefined) {
        uri = uri + `&from=${from}`;
    }

    return fetch(uri)
        .then(x => x.json());
};


const publishImpl = events =>
    fetch(`${EVENT_STORE_URI}/api/publish`, {
        method: 'post',
        body: JSON.stringify(events),
        headers: {'Content-Type': 'application/json'}
    });


export let events =
    undefined;


export let publish =
    undefined;


export const __reset_all__ = () => {
    events = eventsImpl;
    publish = publishImpl;
};

__reset_all__();