import * as hapi from "@hapi/hapi";

import * as CommandAPI from "./CommandAPI"


export const start = configuration => {
    const init = async () => {
        const info =
            configuration.info || false;

        delete configuration.info;

        const server =
            new hapi.Server(configuration);

        await server.register({
            plugin: require('@hapi/inert')
        });

        CommandAPI.register(server);

        await server.start();

        if (info) {
            console.log('News server running at:', server.info.uri);
        }

        return server;
    };


    const server =
        init();

    return server.then(() => ({
        stop: options =>
            server
                .then(s => s.stop(options))
    }));
};
