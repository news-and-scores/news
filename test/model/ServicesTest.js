import * as Services from "../../src/model/Services"
import * as EventStore from "../../src/port/EventStore"
import * as Errors from "../../src/Errors"

import {expect} from "chai"


describe('registerFeed', () => {
    it('when a feed does not exist then the required events are published', () => {
        let publishedEvents =
            undefined;

        Services.__reset_all__();
        EventStore.__reset_all__();

        EventStore.events = () =>
            Promise.resolve([]);

        EventStore.publish = events =>
            publishedEvents = events;

        return Services.registerFeed({
            feedID: 'bbc:top',
            source: 'BBC News',
            title: 'Top Stories',
            description: 'BBC top stories',
            link: 'https://www.bbc.co.uk/news/',
            copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
            image: {
                url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                title: 'BBC News - Africa',
                link: 'https://www.bbc.co.uk/news/'
            }
        }).finally(() => {
            expect(publishedEvents).to.deep.equal([{
                name: 'FeedRegistered',
                version: '1',
                data: {
                    feedID: 'bbc:top',
                    source: 'BBC News',
                    title: 'Top Stories',
                    description: 'BBC top stories',
                    link: 'https://www.bbc.co.uk/news/',
                    copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
                    image: {
                        url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                        title: 'BBC News - Africa',
                        link: 'https://www.bbc.co.uk/news/'
                    }
                }
            }]);
        });
    });

    it('when a feed does exist then an error is returned and no events are published', () => {
        let registerFeedResult =
            undefined;

        let publishedEvents =
            undefined;

        Services.__reset_all__();
        EventStore.__reset_all__();

        EventStore.events = () =>
            Promise.resolve([{
                id: '123',
                name: 'FeedRegistered',
                version: '1',
                data: {
                    feedID: 'bbc:top',
                    source: 'BBC News',
                    title: 'Top Stories',
                    description: 'BBC top stories',
                    link: 'https://www.bbc.co.uk/news/',
                    copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
                    image: {
                        url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                        title: 'BBC News - Africa',
                        link: 'https://www.bbc.co.uk/news/'
                    }
                }
            }]);

        EventStore.publish = events =>
            publishedEvents = events;


        const payload = {
            feedID: 'bbc:top',
            source: 'BBC News',
            title: 'Top Stories',
            description: 'BBC top stories',
            link: 'https://www.bbc.co.uk/news/',
            copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
            image: {
                url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                title: 'BBC News - Africa',
                link: 'https://www.bbc.co.uk/news/'
            }
        };

        return Services.registerFeed(payload)
            .catch(r => registerFeedResult = r)
            .finally(() => {
                expect(publishedEvents).to.equal(undefined);
                expect(registerFeedResult).to.deep.equal(Errors.duplicateFeedID('bbc:top', payload))
            });
    });
});


describe('recordArticle', () => {
    it('when an article does not exist then the required events are published', () => {
        let publishedEvents =
            undefined;

        Services.__reset_all__();
        EventStore.__reset_all__();

        EventStore.events = () =>
            Promise.resolve([{
                id: '123',
                name: 'FeedRegistered',
                version: '1',
                data: {
                    feedID: 'bbc:top',
                    source: 'BBC News',
                    title: 'Top Stories',
                    description: 'BBC top stories',
                    link: 'https://www.bbc.co.uk/news/',
                    copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
                    image: {
                        url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                        title: 'BBC News - Africa',
                        link: 'https://www.bbc.co.uk/news/'
                    }
                }
            }]);

        EventStore.publish = events =>
            Promise.resolve(publishedEvents = events);


        return Services
            .recordArticle({
                name: "RecordArticle",
                articleID: 'bbc:top:091823',
                title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
                description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
                url: {
                    link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                    permanent: true
                },
                publishedDate: new Date(1571213952000).toISOString(),
                feedID: 'bbc:top'
            }).finally(() => {
                expect(publishedEvents.length).to.equal(1);
                expect(publishedEvents).to.deep.equal([{
                    name: 'ArticlePublished',
                    version: '1',
                    data: {
                        articleID: 'bbc:top:091823',
                        title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
                        description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
                        url: {
                            link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                            permanent: true
                        },
                        publishedDate: new Date(1571213952000).toISOString(),
                        feedID: 'bbc:top'
                    }
                }]);
            });
    });

    it('when the article\'s corresponding feed has not been registered then an error is returned', () => {
        let recordArticleResult =
            undefined;

        let publishedEvents =
            undefined;

        Services.__reset_all__();
        EventStore.__reset_all__();

        EventStore.events = () =>
            Promise.resolve([]);

        EventStore.publish = events =>
            Promise.resolve(publishedEvents = publishedEvents.concat(events));


        const payload = {
            name: "RecordArticle",
            articleID: 'bbc:top:091823',
            title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
            description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
            url: {
                link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                permanent: true
            },
            publishedDate: new Date(1571213952000).toISOString(),
            feedID: 'bbc:top'
        };

        return Services
            .recordArticle(payload)
            .catch(r => recordArticleResult = r)
            .finally(() => {
                expect(publishedEvents).to.equal(undefined);
                expect(recordArticleResult).to.deep.equal(Errors.unknownFeedID('bbc:top', payload))
            });
    });

    it('when the article already exists then an error is returned', () => {
        let recordArticleResult =
            undefined;

        let publishedEvents =
            undefined;

        Services.__reset_all__();
        EventStore.__reset_all__();

        EventStore.events = () =>
            Promise.resolve([
                {
                    id: '123',
                    name: 'FeedRegistered',
                    version: '1',
                    data: {
                        feedID: 'bbc:top',
                        source: 'BBC News',
                        title: 'Top Stories',
                        description: 'BBC top stories',
                        link: 'https://www.bbc.co.uk/news/',
                        copyright: 'Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.',
                        image: {
                            url: 'https://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif',
                            title: 'BBC News - Africa',
                            link: 'https://www.bbc.co.uk/news/'
                        }
                    }
                },
                {
                    id: '124',
                    name: 'ArticlePublished',
                    version: '1',
                    data: {
                        articleID: 'bbc:top:091823',
                        title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
                        description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
                        url: {
                            link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                            permanent: true
                        },
                        publishedDate: new Date(1571213952000).toISOString(),
                        feedID: 'bbc:top'
                    }
                }
            ]);

        EventStore.publish = events =>
            Promise.resolve(publishedEvents = events);


        const payload = {
            name: "RecordArticle",
            articleID: 'bbc:top:091823',
            title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
            description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
            url: {
                link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                permanent: true
            },
            publishedDate: new Date(1571213952000).toISOString(),
            feedID: 'bbc:top'
        };

        return Services
            .recordArticle(payload)
            .catch(r => recordArticleResult = r)
            .finally(() => {
                expect(publishedEvents).to.equal(undefined);
                expect(recordArticleResult).to.deep.equal(Errors.duplicateArticleID('bbc:top:091823', payload));
            });
    });
});
