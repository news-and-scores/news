import * as API from "../../src/port/API"
import * as Services from "../../src/model/Services"

import fetch from 'node-fetch'

import {expect} from "chai"


const PORT =
    8000;


describe('/api/command', () => {
    it('when passed a valid register feed command the corresponding service is called', () =>
        validateAPI({
            name: "RegisterFeed",
            feedID: 'bbc:top',
            source: 'BBC News',
            title: 'Top Stories'
        })
    );

    it('when passed a valid record article command the corresponding service is called', () =>
        validateAPI({
            name: "RecordArticle",
            articleID: 'bbc:top:091823',
            title: 'Hong Kong\'s Carrie Lam abandons speech after protests',
            description: 'City leader Carrie Lam\'s annual address is suspended as legislators project slogans and wave placards.',
            url: {
                link: "https://www.bbc.co.uk/news/world-asia-china-50065292",
                permanent: true
            },
            publishedDate: new Date(1571213952000).toISOString(),
            feedID: 'bbc:top'
        })
    );

    it('when passed a command with no name an error is reported', () => errorAPI({},
        {
            type: 'NoNameSuppliedInCommand',
            payload: {}
        }));

    it('when passed a command with a name but not handler associated with that name an error is reported', () => errorAPI({
            name: 'SomeCommand'
        },
        {
            type: 'NoCommandHandlerFound',
            name: 'SomeCommand',
            payload: {
                name: 'SomeCommand'
            }
        }));

    it('when passed a command with a valid handler name but when data that breaks the schema an error is reported', () => errorAPI({
            name: "RegisterFeed",
            feedID: 'bbc:top',
            title: 'Top Stories'
        },
        {
            type: 'HandlerSchemaErrors',
            payload: {
                name: "RegisterFeed",
                feedID: 'bbc:top',
                title: 'Top Stories'
            },
            errors: [
                {
                    dataPath: '',
                    keyword: 'required',
                    message: 'should have required property \'source\'',
                    params: {
                        missingProperty: 'source'
                    },
                    schemaPath: '#/required'
                }
            ]
        }))
});


const validateAPI = (input) =>
    callAPI(input, 200, input);


const errorAPI = (input, error) =>
    callAPI(input, 412, error);


const callAPI = (input, statusCode, apiResult) => {
    let result =
        undefined;

    let cmd =
        undefined;

    Services.__reset_all__();

    Services.registerFeed = c =>
        cmd = c;
    Services.recordArticle = c =>
        cmd = c;

    return API.start({host: 'localhost', port: PORT})
        .then(server =>
            fetch(`http://localhost:${PORT}/api/command`, {
                method: 'post',
                body: JSON.stringify(input),
                headers: {'Content-Type': 'application/json'}
            })
                .then(r => {
                    result = r;
                    return r.json();
                })
                .then(json => {
                    cmd = json
                })
                .then(() => server.stop()))
        .finally(() => {
            expect(result.status).to.equal(statusCode);
            expect(cmd).to.deep.equal(apiResult);
        });
};